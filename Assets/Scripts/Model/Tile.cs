using System;

public abstract class Tile
{
    public abstract TileType TileType { get; }

    private int _id;

    public int Id => _id;

    public event Action<Player> TileEntered;

    public Tile(int id)
    {
        _id = id;
    }

    public virtual void Enter(Player player)
    {
        TileEntered?.Invoke(player);
    }
}

public enum TileType
{
    Start = 0,
    Reward = 1,
    Property = 2
}