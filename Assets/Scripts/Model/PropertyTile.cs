using System;

public class PropertyTile : Tile
{
    private Direction _dir;
    protected int _price;
    protected int _fine;
    private Player _owner;

    public int Price => _price;
    public int Fine => _fine;
    public Direction Direction => _dir;

    public override TileType TileType => TileType.Property;

    public Player Owner
    {
        get => _owner;
        protected set
        {
            if (_owner != value)
            {
                _owner = value;
            }
        }
    }

    public event Action<Player> Purchased;
    public event Action<Player, Player, int> FinePaid;

    public PropertyTile(int id, Direction dir, int price, int fine) : base(id)
    {
        _dir = dir;
        _price = price;
        _fine = fine;
    }

    public override void Enter(Player player)
    {
        base.Enter(player);

        if (Owner == null)
        {
            if (player.TryPayPrice(Price))
            {
                Owner = player;
                Purchased?.Invoke(Owner);
            }
        }
        else if (Owner == player)
        {
            // nothing to do
        }
        else
        {
            // pay fine
            int paidFine = player.PayFine(_fine);
            Owner.AddMoney(paidFine);
            FinePaid?.Invoke(player, Owner, paidFine);
        }
    }
}