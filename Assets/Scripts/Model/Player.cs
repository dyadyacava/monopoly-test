using System;

public class Player
{
    private PlayerSettings _settings;

    private int _money;
    private Tile _tile;

    public int Money
    {
        get => _money;
        private set
        {
            if (_money != value)
            {
                _money = value;
                _moneyChangedEvent?.Invoke(_money);
            }
        }
    }

    public Tile Tile => _tile;

    public PlayerSettings Settings => _settings;

    public event Action<Tile, bool> TileChanged;

    public Action<int> _moneyChangedEvent;
    public event Action<int> MoneyChanged
    {
        add
        {
            lock (this)
            {
                _moneyChangedEvent += value;
                value.Invoke(Money);
            }
        }
        remove
        {
            lock (this)
            {
                _moneyChangedEvent -= value;
            }
        }
    }

    public Player(PlayerSettings settings)
    {
        _settings = settings;
        Money = _settings.StartMoney;
    }

    public int PayFine(int fine)
    {
        int paidFine = _money >= fine ? fine : _money;
        Money -= paidFine;
        return paidFine;
    }

    public bool TryPayPrice(int price)
    {
        if (Money >= price)
        {
            Money -= price;
            return true;
        }

        return false;
    }

    public void AddMoney(int value)
    {
        Money += value;
    }

    public void EnterCurrentTile()
    {
        _tile?.Enter(this);
    }

    public void GoToTile(Tile nextTile, bool isSilent = false)
    {
        _tile = nextTile;
        TileChanged?.Invoke(nextTile, isSilent);
    }
}