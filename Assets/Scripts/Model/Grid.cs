using System.Collections.Generic;

public class Grid
{
    private GridSettings _settings;
    private List<Tile> _tiles;
    private List<TileViewModel> _tileViewModels;

    private int _tilesAmount = 0;
    private RewardRandomizer _randomizer;

    public IReadOnlyList<TileViewModel> TilesViewModels => _tileViewModels;

    public Grid(GridSettings settings)
    {
        _settings = settings;
        _randomizer = new RewardRandomizer(_settings.BaseReward, _settings.RewardWeights);
        GenerateTiles();
    }

    public Tile GetTile(int id)
    {
        if (_tilesAmount > id)
        {
            return _tiles[id];
        }
        return null; // @TODO add assert
    }

    public TileViewModel GetTileViewModel(int id)
    {
        if (_tilesAmount > id)
        {
            return _tileViewModels[id];
        }
        return null; // @TODO add assert
    }

    public int GetTileIdWithOffset(int pos, int offset)
    {
        int sum = pos + offset;
        int id = sum % _tilesAmount;
        return id;
    }

    private void GenerateTiles()
    {
        _tilesAmount = _settings.Width * 2 + _settings.Height * 2 - 4;
        _tiles = new List<Tile>(_tilesAmount);
        _tileViewModels = new List<TileViewModel>(_tilesAmount);

        GenerateTilesSide(_settings.Width, Direction.Bottom, true);
        GenerateTilesSide(_settings.Height, Direction.Left);
        GenerateTilesSide(_settings.Width, Direction.Top);
        GenerateTilesSide(_settings.Height, Direction.Right);
    }
    
    private void GenerateTilesSide(int sideLenght, Direction dir, bool isFirstSide = false)
    {
        for (int i = 0; i < sideLenght - 1; i++)
        {
            int currentId = _tiles.Count;
            Tile tile = null;
            TileViewModel viewModel = null;
            if (i == 0)
            {
                if (isFirstSide)
                {
                    var startTile = new StartTile(currentId, dir, _settings.StartReward);
                    tile = startTile;
                    viewModel = new StartTileViewModel(startTile);
                }
                else
                {
                    var rewardTile = new RewardTile(currentId, dir, _randomizer);
                    tile = rewardTile;
                    viewModel = new RewardTileViewModel(rewardTile);
                }
            }
            else
            {
                var propertyTile = new PropertyTile(currentId, dir, _settings.TilePrice, _settings.Fine);
                tile = propertyTile;
                viewModel = new PropertyTileViewModel(propertyTile);
            }

            _tiles.Add(tile);
            _tileViewModels.Add(viewModel);
        }
    }
}

public enum Direction
{
    Top,
    Bottom,
    Right,
    Left
}