using System;

public class RewardTile : Tile
{
    private RewardRandomizer _randomizer;

    public override TileType TileType => TileType.Reward;

    public event Action<Player, int> RewardGenerated;

    public RewardTile(int id, Direction dir, RewardRandomizer randomizer) : base(id)
    {
        _randomizer = randomizer;
    }

    public override void Enter(Player player)
    {
        base.Enter(player);

        int reward = _randomizer.GetWeightedReward();// UnityEngine.Random.Range(100, 201);
        player.AddMoney(reward);

        RewardGenerated?.Invoke(player, reward);
    }
}
