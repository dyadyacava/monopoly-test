public class StartTile : Tile
{
    private int _startReward;

    public override TileType TileType => TileType.Start;

    public StartTile(int id, Direction dir, int startReward) : base(id)
    {
        _startReward = startReward;
    }

    public override void Enter(Player player)
    {
        base.Enter(player);

        player.AddMoney(_startReward);
    }
}
