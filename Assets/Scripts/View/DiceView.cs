using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DiceView : MonoBehaviour
{
    [SerializeField] private Image _diceIcon;
    [SerializeField] private Image _diceValueIcon;
    [SerializeField] private Button _button;
    [SerializeField] private List<Sprite> _valueSprites;

    [Inject] private GameSession _gameSession;
    [Inject] private TurnSystem _turnSystem;

    private void Start()
    {
        _gameSession.AddSessionInitializedListener(OnSessionInitialized, SessionInitializationPriority.DiceView);
        _turnSystem.TurnStarted += OnTurnStarted;
        _gameSession.ActivePlayerSwitched += OnActivePlayerSwitched;
        _button.onClick.AddListener(OnBtnClick);
    }

    private void OnDestroy()
    {
        _gameSession.RemoveSessionInitializedListener(OnSessionInitialized);
        _turnSystem.TurnStarted -= OnTurnStarted;
        _gameSession.ActivePlayerSwitched -= OnActivePlayerSwitched;
        _button.onClick.RemoveListener(OnBtnClick);
    }

    private void OnSessionInitialized()
    {
        _diceValueIcon.enabled = false;
        _button.interactable = true;
        _diceIcon.color = _gameSession.GetActivePlayerViewModel().Color;
    }

    private void OnTurnStarted(int value)
    {
        _diceValueIcon.enabled = true;
        _button.interactable = false;
        _diceValueIcon.sprite = _valueSprites[value - 1];
    }

    private void OnActivePlayerSwitched(PlayerViewModel playerViewModel)
    {
        _diceIcon.color = playerViewModel.Color;
        _diceValueIcon.enabled = false;
        _button.interactable = true;
    }

    private void OnBtnClick()
    {
        if (!_turnSystem.IsInTurn && !_gameSession.IsGameFinished)
        {
            _turnSystem.MakeTurn();
        }
    }
}
