using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GridView : MonoBehaviour
{
    [SerializeField] private GridLayoutGroup _gridLayout;
    [SerializeField] private float _screenOffset;
    [SerializeField] RectTransform _centerPanel;

    [Inject] private GameSession _gameSession;

    private List<TileView> _tileViews = new List<TileView>();

    public TileView GetTileView(int id)
    {
        return _tileViews[id];
    }

    protected void Start()
    {
        _gridLayout.enabled = true;

        _gameSession.AddSessionInitializedListener(OnSessionInitialized, SessionInitializationPriority.GridView);

        // destroy all children
        while (_gridLayout.transform.childCount != 0)
        {
            DestroyImmediate(_gridLayout.transform.GetChild(0).gameObject);
        }

        float cellWidth = (Screen.width - _screenOffset * 2) / _gameSession.GridSettings.Width - _gridLayout.spacing.x;
        _gridLayout.cellSize = new Vector2(cellWidth, cellWidth);

        var startTile = Instantiate(_gameSession.GridSettings.StartTilePrefab, _gridLayout.transform, false);
        _tileViews.Add(startTile);

        int width = _gameSession.GridSettings.Width;
        int height = _gameSession.GridSettings.Height;

        List<TileView> tilesToDelete = new List<TileView>();
        List<TileView> rightTiles = new List<TileView>();
        List<TileView> leftTiles = new List<TileView>();

        // bottom
        for (int i = 1; i < width - 1; i++)
        {
            var tile = Instantiate(_gameSession.GridSettings.PropertyTilePrefab, _gridLayout.transform, false);
            _tileViews.Add(tile);
        }

        var rewardTile = Instantiate(_gameSession.GridSettings.RewardTilePrefab, _gridLayout.transform, false);
        _tileViews.Add(rewardTile);

        for (int i = 1; i < height - 1; i++)
        {
            for (int j = 0; j < width; j++)
            {
                var propertyTile = Instantiate(_gameSession.GridSettings.PropertyTilePrefab, _gridLayout.transform, false);
                
                if (j == 0)
                {
                    rightTiles.Add(propertyTile);
                }
                else if (j == width - 1)
                {
                    leftTiles.Add(propertyTile);
                }
                else
                {
                    tilesToDelete.Add(propertyTile);
                }
            }                
        }

        // left
        _tileViews.AddRange(leftTiles);

        // top
        for (int i = 0; i < width; i++)
        {
            TileView tile = null;
            if (i == 0 || i == width - 1)
            {
                tile = Instantiate(_gameSession.GridSettings.RewardTilePrefab, _gridLayout.transform, false);
            }
            else
            {
                tile = Instantiate(_gameSession.GridSettings.PropertyTilePrefab, _gridLayout.transform, false);
            }
            _tileViews.Add(tile);
        }

        // right
        rightTiles.Reverse();
        _tileViews.AddRange(rightTiles);
        
        // @TODO remove unused tiles from the center
        foreach (var tileToDelete in tilesToDelete)
        {
            tileToDelete.Disable();
        }

        float centerPanelScale = (cellWidth * (width - 2)) / _centerPanel.sizeDelta.x;
        _centerPanel.localScale = new Vector3(centerPanelScale, centerPanelScale, 1);
    }

    protected void OnDestroy()
    {
        _gameSession.RemoveSessionInitializedListener(OnSessionInitialized);
    }

    private void OnSessionInitialized()
    {
        int size = _tileViews.Count;
        for (int i = 0; i < size; i++)
        {
            _tileViews[i].AttachViewModel(_gameSession.Grid.GetTileViewModel(i));
        }
    }
}
