using UnityEngine;
using UnityEngine.UI;

public class TileEnteredPopup : Popup
{
    [SerializeField] private Text _mainText;
    [SerializeField] private Text _additionalText;

    protected override SessionInitializationPriority _priority => SessionInitializationPriority.TileEnteredPopup;

    protected override void SubscribeOnGameSession()
    {
        base.SubscribeOnGameSession();

        var tileVMList = _gameSession.Grid.TilesViewModels;

        foreach (var vm in tileVMList)
        {
            switch (vm.TileType)
            {
                case TileType.Property:
                    var propertyVM = vm as PropertyTileViewModel;
                    propertyVM.Purchased += OnPurchased;
                    propertyVM.FinePaid += OnFinePaid;
                    break;
                case TileType.Reward:
                    var rewardVM = vm as RewardTileViewModel;
                    rewardVM.Rewarded += OnRewarded;
                    break;
            }
        }
    }

    protected override void UnsubscribeFromGameSession()
    {
        base.UnsubscribeFromGameSession();

        var tileVMList = _gameSession.Grid.TilesViewModels;

        foreach (var vm in tileVMList)
        {
            switch (vm.TileType)
            {
                case TileType.Property:
                    var propertyVM = vm as PropertyTileViewModel;
                    propertyVM.Purchased -= OnPurchased;
                    propertyVM.FinePaid -= OnFinePaid;
                    break;
                case TileType.Reward:
                    var rewardVM = vm as RewardTileViewModel;
                    rewardVM.Rewarded -= OnRewarded;
                    break;
            }
        }
    }

    private void OnPurchased(string playerName)
    {
        OpenPopup();

        _mainText.text = $"PROPERTY PURCHASED";
        _additionalText.text = $"{playerName} purchased a property";
    }

    private void OnFinePaid(string playerName, string ownerName, int fine)
    {
        OpenPopup();

        _mainText.text = $"LOSE {fine}$";
        _additionalText.text = $"{playerName} landed on {ownerName}'s property";
    }

    private void OnRewarded(string playerName, int reward)
    {
        OpenPopup();

        _mainText.text = $"REWARD {reward}$";
        _additionalText.text = $"{playerName} receives a random reward";
    }
}
