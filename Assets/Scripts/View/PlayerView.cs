using UnityEngine;

public class PlayerView : MonoBehaviour
{
    protected PlayerViewModel _viewModel;

    private DeferredInvocationHandler _handler;

    public void AttachViewModel(PlayerViewModel viewModel)
    {
        UnsubscribeViewModel();
        _viewModel = viewModel;
        SubscribeViewModel();
    }

    protected void SubscribeViewModel()
    {
        if (_viewModel != null)
        {
            _viewModel.TurnStarted += OnTurnStarted;
        }
    }

    protected void UnsubscribeViewModel()
    {
        if (_viewModel != null)
        {
            _viewModel.TurnStarted -= OnTurnStarted;
        }
    }

    private void OnTurnStarted(DeferredInvocationHandler handler, TileView nextTileView, bool isSilent)
    {
        Vector3 pos = new Vector3(nextTileView.transform.position.x, nextTileView.transform.position.y, 0);

        if (isSilent)
        {
            transform.position = pos;
        }
        else
        {
            _handler = handler;
            _handler.Lock();
            var descr = LeanTween.move(gameObject, pos, 1f);
            descr.setOnComplete(() => _handler.Unlock());
        }
    }
}