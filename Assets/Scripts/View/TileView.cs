using UnityEngine;
using UnityEngine.UI;

public class TileView : MonoBehaviour
{
    private TileViewModel _viewModel;

    [SerializeField] protected Image _image;

    private void OnValidate()
    {
        _image = GetComponent<Image>();
    }

    public virtual void AttachViewModel(TileViewModel viewModel)
    {
        UnsubscribeViewModel();
        _viewModel = viewModel;
        SubscribeViewModel();
    }

    protected virtual void SubscribeViewModel()
    {
    }

    protected virtual void UnsubscribeViewModel()
    {
    }

    protected virtual void SetupView()
    {
    }

    public virtual void Disable()
    {
        _image.enabled = false;
    }
}
