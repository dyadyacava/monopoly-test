using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Popup : MonoBehaviour
{
    [SerializeField] private Button _okBtn;

    [Inject] protected GameSession _gameSession;

    protected virtual SessionInitializationPriority _priority => SessionInitializationPriority.Popup;

    private void Start()
    {
        ClosePopup();

        _okBtn.onClick.AddListener(OnOkBtnClicked);
        _gameSession.AddSessionInitializedListener(OnSessionInitialized, _priority);
    }

    private void OnSessionInitialized()
    {
        SubscribeOnGameSession();
    }

    private void OnDestroy()
    {
        _okBtn.onClick.RemoveListener(OnOkBtnClicked);
        _gameSession.RemoveSessionInitializedListener(OnSessionInitialized);
        UnsubscribeFromGameSession();
    }

    protected virtual void SubscribeOnGameSession()
    {

    }

    protected virtual void UnsubscribeFromGameSession()
    {

    }

    protected void OpenPopup()
    {
        gameObject.SetActive(true);
    }

    private void ClosePopup()
    {
        gameObject.SetActive(false);
    }

    protected virtual void OnOkBtnClicked()
    {
        ClosePopup();
    }
}
