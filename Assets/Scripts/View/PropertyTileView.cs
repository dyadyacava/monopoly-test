using UnityEngine;
using UnityEngine.UI;

public class PropertyTileView : TileView
{
    private PropertyTileViewModel _propertyViewModel;

    private void OnValidate()
    {
        _image = GetComponent<Image>();
    }

    // @TODO use generics
    public override void AttachViewModel(TileViewModel viewModel)
    {
        _propertyViewModel = viewModel as PropertyTileViewModel;

        base.AttachViewModel(viewModel);
    }

    protected override void SetupView()
    {
        base.SetupView();

        SetupColor();
    }

    protected override void SubscribeViewModel()
    {
        base.SubscribeViewModel();

        if (_propertyViewModel != null)
        {
            _propertyViewModel.Owner.ValueChanged += OnOwnerChanged;
            SetupView();
        }
    }

    protected override void UnsubscribeViewModel()
    {
        base.UnsubscribeViewModel();

        if (_propertyViewModel != null)
        {
            _propertyViewModel.Owner.ValueChanged -= OnOwnerChanged;
        }
    }

    private void OnOwnerChanged(Player newOwner)
    {
        _image.color = _propertyViewModel.Color;
    }

    private void SetupColor()
    {
        Direction dir = _propertyViewModel.Direction;
        switch (dir)
        {
            case Direction.Bottom:
                transform.Rotate(0, 0, 0);
                break;
            case Direction.Right:
                transform.Rotate(0, 0, 90);
                break;
            case Direction.Top:
                transform.Rotate(0, 0, 180);
                break;
            case Direction.Left:
                transform.Rotate(0, 0, 270);
                break;
        }
    }
}
