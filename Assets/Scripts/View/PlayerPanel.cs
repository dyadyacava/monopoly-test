using UnityEngine;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour
{
    [SerializeField] private Text _name;
    [SerializeField] private Text _money;
    [SerializeField] private RectTransform _nowPlayingPanel;

    protected PlayerViewModel _viewModel;

    private void Awake()
    {
        _nowPlayingPanel.gameObject.SetActive(false);
    }

    public void AttachViewModel(PlayerViewModel viewModel)
    {
        UnsubscribeViewModel();
        _viewModel = viewModel;
        SubscribeViewModel();
    }

    protected void SetupView()
    {

    }

    protected void SubscribeViewModel()
    {
        if (_viewModel != null)
        {
            _viewModel.Activated += OnActivated;
            _viewModel.TurnFinished += OnTurnFinished;
            _viewModel.Money.ValueChanged += OnMoneyChanged;

            SetupView();
        }
    }

    protected void UnsubscribeViewModel()
    {
        if (_viewModel != null)
        {
            _viewModel.Activated -= OnActivated;
            _viewModel.TurnFinished -= OnTurnFinished;
            _viewModel.Money.ValueChanged -= OnMoneyChanged;
        }
    }

    private void OnActivated()
    {
        _nowPlayingPanel.gameObject.SetActive(true);
    }

    private void OnTurnFinished()
    {
        _nowPlayingPanel.gameObject.SetActive(false);
    }

    private void OnMoneyChanged(int money)
    {
        _money.text = $"{money}$";
    }
}
