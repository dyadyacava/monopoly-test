using UnityEngine;
using UnityEngine.UI;

public class FinishPopup : Popup
{
    [SerializeField] private Text _additionalText;

    protected override SessionInitializationPriority _priority => SessionInitializationPriority.FinishPopup;

    protected override void SubscribeOnGameSession()
    {
        base.SubscribeOnGameSession();

        _gameSession.GameFinished += OnGameFinished;
    }

    protected override void UnsubscribeFromGameSession()
    {
        base.UnsubscribeFromGameSession();

        _gameSession.GameFinished -= OnGameFinished;
    }

    private void OnGameFinished(PlayerViewModel viewModel)
    {
        OpenPopup();
        _additionalText.text = $"{viewModel.Name} has no money";
    }

    protected override void OnOkBtnClicked()
    {
        base.OnOkBtnClicked();

        _gameSession.StartGame();
    }
}
