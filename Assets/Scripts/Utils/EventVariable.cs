using System;

public class EventVariable<T>
{
    private T _value;
    private bool _invokeOnSubscribe = false;

    public T Value
    {
        get
        {
            return _value;
        }
        set
        {
            if (_value == null && value == null)
                return;

            if (_value != null && _value.Equals(value))
                return;

            _value = value;
            _myEvent?.Invoke(_value);
        }
    }

    private Action<T> _myEvent;
    public event Action<T> ValueChanged
    {
        add
        {
            lock (this)
            {
                _myEvent += value;
                if (_invokeOnSubscribe)
                {
                    value.Invoke(Value);
                }
            }
        }
        remove
        {
            lock (this)
            {
                _myEvent -= value;
            }
        }
    }

    public EventVariable(T value, bool invokeOnSubscribe = false)
    {
        _value = value;
        _invokeOnSubscribe = invokeOnSubscribe;
    }

    public void ForceInvoke()
    {
        _myEvent?.Invoke(_value);
    }
}