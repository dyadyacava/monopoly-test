using System;

public class DeferredInvocationHandler
{
    private int _lockersAmount = 0;

    public bool IsLocked => _lockersAmount > 0;

    public event Action Unlocked;

    public void Lock()
    {
        _lockersAmount++;
    }

    public void Unlock()
    {
        _lockersAmount--;
        if (_lockersAmount == 0)
        {
            Unlocked?.Invoke();
        }
    }
}