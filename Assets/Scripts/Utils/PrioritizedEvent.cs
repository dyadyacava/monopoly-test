using System;
using System.Collections.Generic;

/**
 * This class was taken here with modifications: 
 * https://forum.unity.com/threads/modify-event-order-is-possible-to-do-this-at-runtime.396285/
 **/
public class PrioritizedEvent
{

    #region Fields

    private SortedSet<Token> _set = new SortedSet<Token>(new TokenComparer());

    #endregion

    #region Methods

    public void AddListener(Action callback, int order)
    {
        _set.Add(new Token()
        {
            Order = order,
            Callback = callback
        });
    }

    public void RemoveListener(Action callback)
    {
        _set.Remove(new Token()
        {
            Callback = callback
        });
    }

    public void Clear()
    {
        _set.Clear();
    }

    public void Raise()
    {
        var e = _set.GetEnumerator();
        while (e.MoveNext())
        {
            e.Current.Callback?.Invoke();
        }
    }

    #endregion

    #region Special Types

    private struct Token
    {
        public int Order;
        public Action Callback;
    }

    private class TokenComparer : IComparer<Token>
    {
        public int Compare(Token x, Token y)
        {
            if (x.Callback == y.Callback)
                return 0;

            return x.Order.CompareTo(y.Order);
        }
    }

    #endregion

}