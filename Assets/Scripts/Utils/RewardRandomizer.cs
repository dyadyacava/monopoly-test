using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RewardRandomizer
{
    private int _baseReward;
    private Dictionary<int, int> _rewards;

    public RewardRandomizer(int baseReward, Dictionary<int, int> rewards)
    {
        _baseReward = baseReward;
        _rewards = rewards;
    }

    private int GetRandomWeightedIndex(int[] weights)
    {
        int elementCount = weights.Length;
        int weightSum = 0;
        for (int i = 0; i < elementCount; i++)
        {
            weightSum += weights[i];
        }

        int index = 0;
        int lastIndex = elementCount - 1;
        while (index < lastIndex)
        {
            if (Random.Range(0, weightSum) < weights[index])
            {
                return index;
            }
            weightSum -= weights[index++];
        }

        return index;
    }

    public int GetWeightedReward()
    {
        int id = GetRandomWeightedIndex(_rewards.Values.ToArray());
        int percents = _rewards.Keys.ElementAt(id);
        int reward = (int)(_baseReward * (percents / 100f));
        return reward;
    }
}
