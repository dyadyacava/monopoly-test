﻿using UnityEditor;

namespace MergeTestGame
{
    [CustomPropertyDrawer(typeof(IntIntDictionary))]
    public class AnySerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }
}