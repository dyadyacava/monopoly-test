using System;
using UnityEngine;

public abstract class TileViewModel
{
    private Tile _tileModel;

    public TileType TileType => _tileModel.TileType;

    public TileViewModel(Tile model)
    {
        _tileModel = model;
    }
}

