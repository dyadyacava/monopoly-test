using System;
using UnityEngine;

public class PlayerViewModel
{
    private Player _playerModel;
    private Tile _nextTile;
    private DeferredInvocationHandler _handler = new DeferredInvocationHandler();
    private GameSession _gameSession;

    public EventVariable<int> Money = new EventVariable<int>(0, true);
    public EventVariable<int> CurrentTileId = new EventVariable<int>(0, true);

    public string Name => _playerModel.Settings.Name;
    public Color Color => _playerModel.Settings.Color;

    public event Action<DeferredInvocationHandler, TileView, bool> TurnStarted;
    public event Action TurnFinished;
    public event Action Activated;

    public PlayerViewModel(Player model, GameSession session)
    {
        _gameSession = session;
        _playerModel = model;

        _playerModel.MoneyChanged += OnMoneyChanged;
        _playerModel.TileChanged += OnTileChanged;
        _handler.Unlocked += OnHandlerUnlocked;
        session.ActivePlayerSwitched += OnActivePlayerSwitched;
    }

    private void OnMoneyChanged(int newValue)
    {
        Money.Value = newValue;
    }

    private void OnHandlerUnlocked()
    {
        _playerModel.EnterCurrentTile();
    }

    private void OnTileEntered(Player player)
    {
        _nextTile.TileEntered -= OnTileEntered;
        TurnFinished?.Invoke();
    }

    private void OnActivePlayerSwitched(PlayerViewModel viewModel)
    {
        if (viewModel == this)
        {
            Activated?.Invoke();
        }
    }

    public void OnTileChanged(Tile tile, bool isSilent)
    {
        _nextTile = tile;
        tile.TileEntered += OnTileEntered;
        var tileView = _gameSession.GridView.GetTileView(tile.Id);
        TurnStarted?.Invoke(_handler, tileView, isSilent);
    }
}