using System;
using UnityEngine;

public class PropertyTileViewModel : TileViewModel
{
    private PropertyTile _model;

    public EventVariable<Player> Owner = new EventVariable<Player>(null, true);
    public bool IsBought => Owner.Value != null;
    public Color Color => IsBought ? Owner.Value.Settings.Color : Color.white;
    public int Price => _model.Price;
    public int Fine => _model.Fine;
    public Direction Direction => _model.Direction;

    public event Action<string> Purchased;
    public event Action<string, string, int> FinePaid;

    public PropertyTileViewModel(PropertyTile model) : base(model)
    {
        _model = model;
        Subscribe();
    }

    protected void Subscribe()
    {
        _model.Purchased += OnPurchased;
        _model.FinePaid += OnFinePaid;
    }

    private void OnPurchased(Player player)
    {
        Owner.Value = player;
        Purchased?.Invoke(player.Settings.Name);
    }

    private void OnFinePaid(Player player, Player owner, int fine)
    {
        FinePaid?.Invoke(player.Settings.Name, owner.Settings.Name, fine);
    }
}
