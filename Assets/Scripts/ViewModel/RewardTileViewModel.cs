using System;

public class RewardTileViewModel : TileViewModel
{
    private RewardTile _model;

    public event Action<string, int> Rewarded;

    public RewardTileViewModel(RewardTile model) : base(model)
    {
        _model = model;
        Subscribe();
    }

    protected void Subscribe()
    {
        _model.RewardGenerated += OnRewardGenerated;
    }

    private void OnRewardGenerated(Player player, int reward)
    {
        Rewarded?.Invoke(player.Settings.Name, reward);
    }
}
