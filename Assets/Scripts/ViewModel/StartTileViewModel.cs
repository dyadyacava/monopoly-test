public class StartTileViewModel : TileViewModel
{
    private StartTile _model;

    public StartTileViewModel(StartTile model) : base(model)
    {
        _model = model;
        Subscribe();
    }

    protected void Subscribe()
    {
    }
}