using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TurnSystem : MonoBehaviour
{
    [Inject] private GameSession _gameSession;
    [Inject] private GridView _gridView;

    private bool _isInTurn = false;
    private Dictionary<int, int> _playersPositions = new Dictionary<int, int>();

    public bool IsInTurn => _isInTurn;

    public event Action<int> TurnStarted;

    private void Start()
    {
        _gameSession.AddSessionInitializedListener(OnSessionInitialized, SessionInitializationPriority.TurnSystem);
    }

    private void OnDestroy()
    {
        _gameSession.RemoveSessionInitializedListener(OnSessionInitialized);
    }

    public void MakeTurn()
    {
        int offset = UnityEngine.Random.Range(1, 7);
        int nextTileId = _gameSession.Grid.GetTileIdWithOffset(_playersPositions[_gameSession.ActivePlayerId], offset);
        _playersPositions[_gameSession.ActivePlayerId] = nextTileId;

        var nextTile = _gameSession.Grid.GetTile(nextTileId);
        var player = _gameSession.GetActivePlayer();

        player.GoToTile(nextTile);
        nextTile.TileEntered += OnTileEntered;

        _isInTurn = true;
        TurnStarted?.Invoke(offset);

        void OnTileEntered(Player playerEntered)
        {
            _isInTurn = false;
            nextTile.TileEntered -= OnTileEntered;

            if (!_gameSession.TryFinishGame())
            {
                _gameSession.SwitchActivePlayer();
            }
        }
    }

    private void OnSessionInitialized()
    {
        for (int i = 0; i < _gameSession.PlayersAmount; i++)
        {
            _playersPositions.Add(i, 0);
        }
    }
}