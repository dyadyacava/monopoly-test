using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameSession : MonoBehaviour
{
    [SerializeField] private GridSettings _gridSettings;
    [SerializeField] private List<PlayerSettings> _playersSettings;

    [Inject] private GridView _gridView;

    public int PlayersAmount => _playersSettings.Count;
    public GridSettings GridSettings => _gridSettings;

    private List<PlayerViewModel> _playerViewModels = new List<PlayerViewModel>();
    private List<Player> _players = new List<Player>();

    private Grid _grid;
    private bool _isGameFinished = false;

    private int _activePlayerId = -1;
    public int ActivePlayerId
    {
        get => _activePlayerId;
        private set
        {
            if (_activePlayerId != value)
            {
                _activePlayerId = value;
                ActivePlayerSwitched?.Invoke(_playerViewModels[value]);
            }
        }
    }

    public Grid Grid => _grid;
    public GridView GridView => _gridView;

    public bool IsGameFinished => _isGameFinished;

    private bool _isSessionInitialized = false;
    public PrioritizedEvent _sessionInitializedEvent = new PrioritizedEvent();

    public void AddSessionInitializedListener(Action callback, SessionInitializationPriority priority = 0)
    {
        _sessionInitializedEvent.AddListener(callback, (int)priority);
        if (_isSessionInitialized)
        {
            callback.Invoke();
        }
    }

    public void RemoveSessionInitializedListener(Action callback)
    {
        _sessionInitializedEvent.RemoveListener(callback);
    }

    public Action<PlayerViewModel> ActivePlayerSwitched;
    public event Action<PlayerViewModel> GameFinished;

    public PlayerViewModel GetActivePlayerViewModel()
    {
        return _playerViewModels[ActivePlayerId];
    }

    public Player GetActivePlayer()
    {
        return _players[ActivePlayerId];
    }

    private void Start()
    {
        // @TODO check null
        _grid = new Grid(_gridSettings);

        for (int i = 0; i < PlayersAmount; i++)
        {
            PlayerSettings settings = _playersSettings[i];

            Player player = new Player(settings);
            _players.Add(player);

            var viewModel = new PlayerViewModel(player, this);
            var view = settings.PlayerView;

            view.AttachViewModel(viewModel);
            settings.PlayerPanel.AttachViewModel(viewModel);

            _playerViewModels.Add(viewModel);

            if (i == 0)
            {
                ActivePlayerId = i;
            }
        }
    }

    // @TODO need to find better solution
    private void Update()
    {
        if (!_isSessionInitialized)
        {
            _isSessionInitialized = true;
            _sessionInitializedEvent?.Raise();

            var startTile = _grid.GetTile(0);

            IEnumerator Routine()
            {
                yield return new WaitForSeconds(0.001f);
                foreach (var player in _players)
                {
                    player.GoToTile(startTile, true);
                }
            }

            StartCoroutine(Routine());            
        }
    }

    private PlayerViewModel FindLoser()
    {
        return _playerViewModels.Find(vm => vm.Money.Value == 0);
    }    

    public void SwitchActivePlayer()
    {
        ActivePlayerId = (ActivePlayerId + 1) % PlayersAmount;
    }

    public bool TryFinishGame()
    {
        var loser = FindLoser();
        if (loser != null)
        {
            GameFinished?.Invoke(loser);
            _isGameFinished = true;
            return true;
        }
        return false;
    }

    public void StartGame()
    {
        _isGameFinished = false;
        ActivePlayerId = 0;
    }
}

[Serializable]
public class GridSettings
{
    public int Width = 7;
    public int Height = 7;

    public TileView StartTilePrefab;
    public TileView RewardTilePrefab;
    public TileView PropertyTilePrefab;

    public int StartReward = 70;
    public int BaseReward = 50;
    public int TilePrice = 100;
    public int Fine = 30;
    public IntIntDictionary RewardWeights;
}

[Serializable]
public class PlayerSettings
{
    public string Name = "Player 0";
    public Color Color = Color.blue;
    public int StartMoney = 1000;
    public PlayerView PlayerView;
    public PlayerPanel PlayerPanel;
}

public enum SessionInitializationPriority
{
    GridView = 0,
    TurnSystem = 10,
    DiceView = 20,
    Popup = 30,
    FinishPopup = 31,
    TileEnteredPopup = 32,
}