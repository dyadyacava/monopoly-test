using Zenject;

public class GameInstaller : MonoInstaller<GameInstaller>
{
    public override void InstallBindings()
    {
        // Systems

        Container.Bind<GameSession>().FromComponentsInHierarchy().AsSingle();
        Container.Bind<GridView>().FromComponentsInHierarchy().AsSingle();
        Container.Bind<TurnSystem>().FromComponentsInHierarchy().AsSingle();
    }
}